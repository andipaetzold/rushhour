import java.util.HashSet;
import java.util.Set;

/**
 * This is a template for the class corresponding to your original advanced
 * heuristic. This class is an implementation of the <tt>Heuristic</tt>
 * interface. After thinking of an original heuristic, you should implement it
 * here, filling in the constructor and the <tt>getValue</tt> method.
 */
public class AdvancedHeuristic implements Heuristic {
    private Puzzle puzzle;
    private boolean goalOrientation;
    private int goalFixedPos;
    private int goalCarSize;

    /**
     * This is the required constructor, which must be of the given form.
     */
    public AdvancedHeuristic(Puzzle puzzle) {
        this.puzzle = puzzle;
        this.goalOrientation = puzzle.getCarOrient(0);
        this.goalFixedPos = puzzle.getFixedPosition(0);
        this.goalCarSize = puzzle.getCarSize(0);
    }

    /**
     * This method returns the value of the heuristic function at the given
     * state.
     */
    @Override
    public int getValue(State state) {
        if (state.isGoal()) return 0;

        int heur = 1;
        Set<Integer> blocking = new HashSet<>();
        for (int varPos = state.getVariablePosition(0) + goalCarSize; varPos < puzzle.getGridSize(); varPos++) {
            for (int carId = 1; carId < puzzle.getNumCars(); carId++) {
                if (isBlocking(state, varPos, goalFixedPos, carId, goalOrientation)) {
                    heur += sumOfBlockingCars(state, carId, varPos, blocking);
                }
            }
        }

        return heur;
    }

    private int sumOfBlockingCars(State state, int carId, int varPos, Set<Integer> blocking) {
        int sum1 = 1;
        for (int i = state.getVariablePosition(carId); i < varPos; i++) {
            for (int j = 1; j < puzzle.getNumCars(); j++) {
                if (!blocking.contains(j) && isBlocking(state, i, puzzle.getFixedPosition(carId), j, puzzle.getCarOrient(carId))) {
                    sum1++;
                    blocking.add(j);
                }
            }
        }

        int sum2 = 1;
        for (int i = state.getVariablePosition(carId) + puzzle.getCarSize(carId) - 1; i > varPos; i--) {
            for (int j = 1; j < puzzle.getNumCars(); j++) {
                if (!blocking.contains(j) && isBlocking(state, i, puzzle.getFixedPosition(carId), j, puzzle.getCarOrient(carId))) {
                    sum2++;
                    blocking.add(j);
                }
            }
        }

        return sum1 > sum2 ? sum1 : sum2;
    }

    private boolean isBlocking(State state, int varPos, int fixedPosition, int carId, boolean orientation) {
        return puzzle.getCarOrient(carId) != orientation &&
                isBetween(state.getVariablePosition(carId), puzzle.getCarSize(carId), fixedPosition) &&
                varPos == puzzle.getFixedPosition(carId);
    }

    private boolean isBetween(int startPosition, int size, int point) {
        int endPosition = startPosition + size - 1;
        return point >= startPosition &&
                point <= endPosition;
    }
}
