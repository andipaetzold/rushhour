import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * This is the template for a class that performs A* search on a given rush hour
 * puzzle with a given heuristic. The main search computation is carried out by
 * the constructor for this class, which must be filled in. The solution (a path
 * from the initial state to a goal state) is returned as an array of
 * <tt>State</tt>s called <tt>path</tt> (where the first element
 * <tt>path[0]</tt> is the initial state). If no solution is found, the
 * <tt>path</tt> field should be set to <tt>null</tt>. You may also wish to
 * return other information by adding additional fields to the class.
 */

public class AStar {

    /**
     * The solution path is stored here
     */
    public State[] path;

    private PriorityQueue<Node> openList;
    private HashMap<State, Node> closedList;
    private HashMap<State, Integer> costMap;

    private Node curNode;
    private Heuristic heuristic;

    /**
     * This is the constructor that performs A* search to compute a solution for
     * the given puzzle using the given heuristic.
     */
    public AStar(Puzzle puzzle, Heuristic heuristic) {
        this.heuristic = heuristic;

        openList = new PriorityQueue<>((n1, n2) -> costMap.get(((Node)n1).getState()) - costMap.get(((Node)n2).getState()));
        closedList = new HashMap<>();
        costMap = new HashMap<>();

        openList.add(puzzle.getInitNode());
        costMap.put(puzzle.getInitNode().getState(), totalNodeCosts(puzzle.getInitNode()));
        
        while (!openList.isEmpty()) {
            curNode = openList.poll();

            if (curNode.getState().isGoal()) {
                path = new State[curNode.getDepth()];

                int i = curNode.getDepth() - 1;
                while (curNode.getParent() != null) {
                    path[i] = curNode.getState();
                    curNode = curNode.getParent();
                    --i;
                }
                return;
            }

            closedList.put(curNode.getState(), curNode);
            checkSuccessorNodes(curNode.expand(), curNode);
        }
    }

    private void checkSuccessorNodes(Node[] successorNodes, Node curNode) {
        for (Node n : successorNodes) {
            if (closedList.containsKey(n.getState())) {
                continue;
            }

            int costs = totalNodeCosts(n);
            if (costMap.containsKey(n.getState()) &&
                    costMap.get(n.getState()) <= costs) {
                continue;
            }

            Integer i = costMap.remove(n.getState());
            costMap.put(n.getState(), costs);
            if (i != null) openList.removeIf(node -> node.getState().equals(n.getState()));
            openList.add(n);
        }
    }

    private int totalNodeCosts(Node node) {
        return node.getDepth() + heuristic.getValue(node.getState());
    }
}