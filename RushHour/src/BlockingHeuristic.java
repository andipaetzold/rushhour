/**
 * This is a template for the class corresponding to the blocking heuristic.
 * This heuristic returns zero for goal states, and otherwise returns one plus
 * the number of cars blocking the path of the goal car to the exit. This class
 * is an implementation of the <tt>Heuristic</tt> interface, and must be
 * implemented by filling in the constructor and the <tt>getValue</tt> method.
 */
public class BlockingHeuristic implements Heuristic {
    private final Puzzle puzzle;
    private final boolean goalOrientation;
    private final int goalFixedPos;
    private final int goalCarSize;

    /**
     * This is the required constructor, which must be of the given form.
     */
    public BlockingHeuristic(Puzzle puzzle) {
        this.puzzle = puzzle;
        this.goalOrientation = puzzle.getCarOrient(0);
        this.goalFixedPos = puzzle.getFixedPosition(0);
        this.goalCarSize = puzzle.getCarSize(0);
    }

    /**
     * This method returns the value of the heuristic function at the given
     * state.
     */
    @Override
    public int getValue(State state) {
        if (state.isGoal()) {
            return 0;
        }

        int heuristic = 1;

        for (int varPos = state.getVariablePosition(0) + goalCarSize; varPos < puzzle.getGridSize(); varPos++) {
            for (int carId = 1; carId < puzzle.getNumCars(); carId++) {
                if (isBlocking(state, varPos, carId)) heuristic++;
            }
        }

        return heuristic;
    }

    private boolean isBlocking(State state, int varPos, int carId) {
        return puzzle.getCarOrient(carId) != goalOrientation &&
                isBetween(state.getVariablePosition(carId), puzzle.getCarSize(carId), goalFixedPos) &&
                varPos == puzzle.getFixedPosition(carId);
    }

    private boolean isBetween(int startPosition, int size, int isBetween) {
        return isBetween >= startPosition && isBetween < startPosition + size;
    }
}
